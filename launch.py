#!/usr/bin/env python
'''\
  Launch aws instances
Usage:
  launch.py NAME [ --userdata USER_DATA_FILE --config_file CONFIG_FILE ]
  launch.py NAME [ --bdm BDM_SIZE --userdata USER_DATA_FILE --config_file CONFIG_FILE ]

Options:
  -h --help                 Display this screen

'''
import yaml
import time
import boto3
import docopt
import os,sys

ACCT_ID = ''
ami_filters = {'owner-id': ACCT_ID}
args = docopt.docopt(__doc__)

ec2 = boto3.resource('ec2', region_name=args.get('region'))
client = boto3.client('ec2')


def get_config(config_file=args.get('CONFIG_FILE')):
    if os.path.isfile(config_file):
        try:
            with open(config_file, 'r') as f:
                return yaml.safe_load(f)
        except:
            print 'Error loading config file. My not be valid yaml.'
            return None
    else:
        print 'Error opening config file: {} no such file'.format(config_file)
        return None


def get_userdata(filename):
    if os.path.isfile(filename):
        with open(filename) as f:
            return f.read()
    else:
        print 'Error opening userdata file {}'
        return None


def launch(launch_params):
    return ec2.create_instances(**launch_params)


def tag_instance(instance):
    instance[0].create_tags(Tags=[{'Key': 'Name', 'Value': args.get('NAME')}])

config = get_config(args.get('CONFIG_FILE'))

config['params']['UserData'] = get_userdata(args.get('USER_DATA_FILE'))

tag_instance(launch(config.get('params')))
